﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;

using System.Data.SqlClient;
using veterinaria_apirest.Model;

namespace veterinaria_apirest.Controllers
{
    public class SqlAccess
    {
        public static SqlConnection conexion = new SqlConnection("" +
            "Server=AAAAAA\\SQLEXPRESS;" +
            "Database=veterinario;" +
            "User Id=rafa;" +
            "Password=aguilar1234567;"
       );

        public static List<ListCitas> IndexCitas()
        {
            var dt = new DataTable();
            List<ListCitas> Li = new List<ListCitas>();
            try
            {
                conexion.Open();
                SqlDataAdapter da = new SqlDataAdapter("select * FROM vista_veterinario;", conexion);
                da.Fill(dt);
                conexion.Close();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ListCitas dato = new ListCitas
                    {
                        Nombre = dt.Rows[i]["nombre"].ToString(),
                        Apellido = dt.Rows[i]["apellido"].ToString(),
                        Email = dt.Rows[i]["email"].ToString(),
                        Telefono = dt.Rows[i]["telefono"].ToString(),
                        Especie_Mascota = dt.Rows[i]["especie_mascota"].ToString(),
                        Nombre_Mascota = dt.Rows[i]["nombre_mascota"].ToString(),
                        Folio = dt.Rows[i]["folio"].ToString(),
                        Fecha = dt.Rows[i]["fecha"].ToString(),
                        Hora = dt.Rows[i]["hora"].ToString(),
                    };
                    Li.Add(dato);
                }
                return Li;
            }
            catch (SqlException)
            {
                conexion.Close();
                return Li;
            }
        }
        public static List<Pacientes> IndexPacientes()
        {
            var dt = new DataTable();
            List<Pacientes> Li = new List<Pacientes>();
            try
            {
                conexion.Open();
                SqlDataAdapter da = new SqlDataAdapter("select * FROM paciente;", conexion);
                da.Fill(dt);
                conexion.Close();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Pacientes dato = new Pacientes
                    {
                        Id = int.Parse(dt.Rows[i]["id"].ToString()),
                        Nombre = dt.Rows[i]["nombre"].ToString(),
                        Apellido = dt.Rows[i]["apellido"].ToString(),
                        Email = dt.Rows[i]["email"].ToString(),
                        Telefono = dt.Rows[i]["telefono"].ToString(),
                        Especie_Mascota = dt.Rows[i]["especie_mascota"].ToString(),
                        Nombre_Mascota = dt.Rows[i]["nombre_mascota"].ToString(),
                    };
                    Li.Add(dato);
                }
                return Li;
            }
            catch (SqlException)
            {
                conexion.Close();
                return Li;
            }
        }
        public static bool InsertCitas(int id_paciente, String fecha, String hora)
        {
            SqlCommand query = new SqlCommand(String.Format("exec insert_paciente " +
                "@_id_paciente = {0}" +
                "@_fecha = '{1}', " +
                "@_hora = '{2}', ",
                id_paciente,
                fecha,
                hora
                ), conexion);
            try
            {
                conexion.Open();
                query.ExecuteNonQuery();
                conexion.Close();
                return true;
            }
            catch (Exception)
            {
                conexion.Close();
                return false;
            }
        }

        public static bool UpdateCitas(int id, int id_paciente, String fecha, String hora)
        {
            SqlCommand query = new SqlCommand(String.Format("exec update_paciente " +
                "@_id_paciente = {0}" +
                "@_fecha = '{1}', " +
                "@_hora = '{2}', " +
                "@_id = {3} ",
                id_paciente,
                fecha,
                hora,
                id
                ), conexion);
            try
            {
                conexion.Open();
                query.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                conexion.Close();
                return false;
            }
        }

        public static bool DeleteCitas(int id)
        {
            SqlCommand query = new SqlCommand(String.Format("exec delete_cita " +
                "@_id = {0}",
                id
                ), conexion);
            try
            {
                conexion.Open();
                query.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                conexion.Close();
                return false;
            }
        }

        public static bool InsertPacientes(String nombre, String apellido, String email, String telefono, String especie_mascota, String nombre_mascota)
        {
            SqlCommand query = new SqlCommand(String.Format("exec insert_paciente " +
                "@_nombre = '{0}', " +
                "@_apellido = '{1}', " +
                "@_email = '{2}', " +
                "@_telefono = '{3}', " +
                "@_especie_mascota = '{4}', " +
                "@_nombre_mascota = '{5}'",
                nombre,
                apellido,
                email,
                telefono,
                especie_mascota,
                nombre_mascota
                ), conexion);
            try
            {
                conexion.Open();
                query.ExecuteNonQuery();
                conexion.Close();
                return true;
            }
            catch (Exception)
            {
                conexion.Close();
                return false;
            }
        }

        public static bool UpdatePacientes(int id, String nombre, String apellido, String email, String telefono, String especie_mascota, String nombre_mascota)
        {
            SqlCommand query = new SqlCommand(String.Format("exec update_paciente " +
                "@_id = {6}" +
                "@_nombre = '{0}', " +
                "@_apellido = '{1}', " +
                "@_email = '{2}', " +
                "@_telefono = '{3}', " +
                "@_especie_mascota = '{4}', " +
                "@_nombre_mascota = '{5}'",
                nombre,
                apellido,
                email,
                telefono,
                especie_mascota,
                nombre_mascota,
                id
                ), conexion);
            try
            {
                conexion.Open();
                query.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                conexion.Close();
                return false;
            }
        }

        public static bool DeletePacientes(int id)
        {
            SqlCommand query = new SqlCommand(String.Format("exec delete_paciente " +
                "@_id = {0}",
                id
                ), conexion);
            try
            {
                conexion.Open();
                query.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                conexion.Close();
                return false;
            }
        }
    }
}
