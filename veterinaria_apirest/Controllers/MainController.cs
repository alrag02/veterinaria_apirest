﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace veterinaria_apirest.Controllers
{

    [ApiController]
    [Route("api")]
    public class MainContoller : Controller
    {
        [HttpGet("cita")]
        public JsonResult IndexCita()
        {
            return Json(SqlAccess.IndexCitas());
        }

        [HttpPost("cita/insert")]
        public JsonResult InsertCita(int id_paciente, String fecha, String hora)
        {
            return Json(SqlAccess.InsertCitas(id_paciente, fecha, hora));
        }

        [HttpPut("cita/edit")]
        public JsonResult EditCita(int id, int id_paciente, String fecha, String hora)
        {
            return Json(SqlAccess.UpdateCitas(id, id_paciente, fecha, hora));
        }

        [HttpDelete("cita/delete")]
        public JsonResult DeleteCita(int id)
        {
            return Json(SqlAccess.DeleteCitas(id));
        }

        [HttpGet("paciente")]
        public JsonResult IndexPaciente()
        {
            return Json(SqlAccess.IndexPacientes());
        }

        [HttpPost("paciente/insert")]
        public JsonResult InsertPaciente(String nombre, String apellido, String email, String telefono, String especie_mascota, String nombre_mascota)
        {
            return Json(SqlAccess.InsertPacientes(nombre, apellido, email, telefono, especie_mascota, nombre_mascota));
        }

        [HttpPut("paciente/edit")]
        public JsonResult EditPaciente(int id, String nombre, String apellido, String email, String telefono, String especie_mascota, String nombre_mascota)
        {
            return Json(SqlAccess.UpdatePacientes(id, nombre, apellido, email, telefono, especie_mascota, nombre_mascota));
        }

        [HttpDelete("paciente/delete")]
        public JsonResult DeletePaciente(int id)
        {
            return Json(SqlAccess.DeletePacientes(id));
        }
    }
}
