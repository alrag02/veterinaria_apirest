﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace veterinaria_apirest.Model
{
    public class ListCitas
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string Especie_Mascota { get; set; }
        public string Nombre_Mascota { get; set; }
        public string Folio { get; set; }
        public string Fecha { get; set; }
        public string Hora { get; set; }
    }
}
