USE [master]
GO
/****** Object:  Database [veterinario]    Script Date: 02/06/2021 17:42:48 ******/
CREATE DATABASE [veterinario]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'veterinario', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\veterinario.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'veterinario_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\veterinario_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [veterinario] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [veterinario].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [veterinario] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [veterinario] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [veterinario] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [veterinario] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [veterinario] SET ARITHABORT OFF 
GO
ALTER DATABASE [veterinario] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [veterinario] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [veterinario] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [veterinario] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [veterinario] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [veterinario] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [veterinario] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [veterinario] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [veterinario] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [veterinario] SET  ENABLE_BROKER 
GO
ALTER DATABASE [veterinario] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [veterinario] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [veterinario] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [veterinario] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [veterinario] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [veterinario] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [veterinario] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [veterinario] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [veterinario] SET  MULTI_USER 
GO
ALTER DATABASE [veterinario] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [veterinario] SET DB_CHAINING OFF 
GO
ALTER DATABASE [veterinario] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [veterinario] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [veterinario] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [veterinario] SET QUERY_STORE = OFF
GO
USE [veterinario]
GO
/****** Object:  Table [dbo].[paciente]    Script Date: 02/06/2021 17:42:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[paciente](
	[id] [int] IDENTITY(0,1) NOT NULL,
	[nombre] [text] NOT NULL,
	[apellido] [text] NOT NULL,
	[email] [text] NOT NULL,
	[telefono] [text] NOT NULL,
	[especie_mascota] [text] NOT NULL,
	[nombre_mascota] [text] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cita]    Script Date: 02/06/2021 17:42:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cita](
	[id] [int] IDENTITY(0,1) NOT NULL,
	[folio] [text] NOT NULL,
	[fecha] [date] NOT NULL,
	[hora] [time](7) NOT NULL,
	[paciente_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[vista_veterinario]    Script Date: 02/06/2021 17:42:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vista_veterinario] AS 
SELECT paciente.nombre, paciente.apellido, paciente.email, paciente.telefono, paciente.especie_mascota, paciente.nombre_mascota, cita.folio, cita.fecha, cita.hora 
FROM cita, paciente
WHERE paciente.id = cita.paciente_id;
GO
ALTER TABLE [dbo].[cita]  WITH CHECK ADD FOREIGN KEY([paciente_id])
REFERENCES [dbo].[paciente] ([id])
GO
/****** Object:  StoredProcedure [dbo].[delete_cita]    Script Date: 02/06/2021 17:42:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[delete_cita]
@_id int
AS
BEGIN
	DELETE cita 
	WHERE id = @_id;
END;
GO
/****** Object:  StoredProcedure [dbo].[delete_paciente]    Script Date: 02/06/2021 17:42:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[delete_paciente]
@_id int
AS
BEGIN
	DELETE paciente WHERE paciente.id = @_id;
END;
GO
/****** Object:  StoredProcedure [dbo].[edit_cita]    Script Date: 02/06/2021 17:42:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[edit_cita]
@_id_paciente int,
@_fecha date,
@_hora time,
@_id int
AS
begin
	UPDATE cita SET  
	paciente_id = @_id_paciente, 
	fecha =  @_fecha,
	hora = @_hora
	WHERE id = @_id;
end;
GO
/****** Object:  StoredProcedure [dbo].[edit_paciente]    Script Date: 02/06/2021 17:42:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[edit_paciente]
@_id int,
@_nombre text,
@_apellido text,
@_email text,
@_telefono text,
@_especie_mascota text,
@_nombre_mascota text
AS
BEGIN
	UPDATE paciente SET  
	nombre = @_nombre, 
	apellido =  @_apellido,
	email = @_email,
	telefono = @_telefono,
	especie_mascota = @_especie_mascota,
	nombre_mascota = @_nombre_mascota
	WHERE id = @_id;
END;
GO
/****** Object:  StoredProcedure [dbo].[insert_cita]    Script Date: 02/06/2021 17:42:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[insert_cita]
@_id_paciente int,
@_fecha date,
@_hora time
AS
BEGIN
	INSERT INTO cita (paciente_id, fecha, hora) VALUES (@_id_paciente, @_fecha, @_hora);
END;
GO
/****** Object:  StoredProcedure [dbo].[insert_paciente]    Script Date: 02/06/2021 17:42:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[insert_paciente]
@_nombre text,
@_apellido text,
@_email text,
@_telefono text,
@_especie_mascota text,
@_nombre_mascota text
AS
BEGIN
	INSERT INTO paciente (nombre, apellido, email, telefono, especie_mascota, nombre_mascota) VALUES (@_nombre, @_apellido, @_email, @_telefono, @_especie_mascota, @_nombre_mascota);
END;
GO
USE [master]
GO
ALTER DATABASE [veterinario] SET  READ_WRITE 
GO
